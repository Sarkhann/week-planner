import java.util.Scanner;

public class Week {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String[][] schedule = new String[7][2];
        schedule[0][0] = "sunday";
        schedule[0][1] = "rest and relax";
        schedule[1][0] = "monday";
        schedule[1][1] = "workout in the morning; study in the evening";
        schedule[2][0] = "tuesday";
        schedule[2][1] = "meet friends for dinner";
        schedule[3][0] = "wednesday";
        schedule[3][1] = "go to the library; attend yoga class";
        schedule[4][0] = "thursday";
        schedule[4][1] = "work on coding project";
        schedule[5][0] = "friday";
        schedule[5][1] = "go for a hike";
        schedule[6][0] = "saturday";
        schedule[6][1] = "visit the art museum";

        boolean check = true;

        while (check) {
            System.out.println("Please, input the day of the week or ('exit' to quit): ");
            String input = sc.nextLine().toLowerCase();
            if (input.equals("exit")) {
                break;
            }

            boolean found = false;

            for (int i = 0; i < 7; i++) {
                if (schedule[i][0].equals(input)) {
                    System.out.println("Your tasks for " + schedule[i][0] + ": " + schedule[i][1]);
                    found = true;
                    break;
                }
            }

            if (!found) {
                System.out.println("Sorry, I don't understand you, please try again.");
            }
        }
    }
}